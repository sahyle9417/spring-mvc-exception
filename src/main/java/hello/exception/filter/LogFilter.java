package hello.exception.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

@Slf4j
public class LogFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("LogFilter init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String requestURI = httpRequest.getRequestURI();
        String uuid = UUID.randomUUID().toString();
        String dispatcherType = request.getDispatcherType().toString();

        try {
            log.info("[{}] [{}] [{}] LogFilter start", requestURI, uuid, dispatcherType);
            // 아래와 같이 chain.doFilter 호출해야 다음 필터 또는 controller 가 정상 실행됨 (누락하면 요청 처리 안됨) (필터 단점)
            chain.doFilter(request, response);
        } catch (Exception e) {
            log.info("[{}] [{}] [{}] LogFilter found exception ({})", requestURI, uuid, dispatcherType, e.getMessage());
            throw e;
        } finally {
            log.info("[{}] [{}] [{}] LogFilter end", requestURI, uuid, dispatcherType);
        }

    }

    @Override
    public void destroy() {
        log.info("LogFilter destroy");
    }
}
