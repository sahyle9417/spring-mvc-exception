package hello.exception.exhandler.advice;

import hello.exception.exception.UserException;
import hello.exception.exhandler.ErrorResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

// ControllerAdvice 는 Advice 를 적용할 Controller 를 지정할 수 있다
// 1. 특정 annotation 을 가진 Controller 에 적용
// -> @ControllerAdvice(annotations = RestController.class)
// 2. 특정 패키지 하위의 Controller 에 적용
// -> @ControllerAdvice("hello.exception.api")
// 3. 특정 Controller 클래스에 적용 (아래 2가지 중 택1)
// -> @ControllerAdvice(assignableTypes = {ApiExceptionV2Controller.class})
// -> 적용할 Controller 클래스 안에 ExceptionHandler 어노테이션 붙은 오류 처리 메서드 추가 
// 4. 모든 Controller 에 적용
// -> @ControllerAdvice
// -> 대상 Controller 미지정 시, 모든 controller 에 적용

@Slf4j
@RestControllerAdvice
public class ExControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public ErrorResult illegalExHandler(IllegalArgumentException e) {
        log.error("illegalExHandler found IllegalArgumentException", e);
        return new ErrorResult("BAD", e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResult> userExHandler(UserException e) {
        log.error("userExHandler found UserException", e);
        ErrorResult errorResult = new ErrorResult("USER-EX", e.getMessage());
        return new ResponseEntity<>(errorResult, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ErrorResult exHandler(Exception e) {
        log.error("exHandler found Exception", e);
        return new ErrorResult("EX", "내부 오류");
    }

}
