package hello.exception.servlet;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
public class ErrorPageController {

    // exception 을 수신한 WAS 는 아래의 key 에 해당하는 key-value 쌍을 request attribute 에 추가
    public static final String ERROR_EXCEPTION = RequestDispatcher.ERROR_EXCEPTION;             // javax.servlet.error.exception
    public static final String ERROR_EXCEPTION_TYPE = RequestDispatcher.ERROR_EXCEPTION_TYPE;   // javax.servlet.error.exception_type
    public static final String ERROR_MESSAGE = RequestDispatcher.ERROR_MESSAGE;                 // javax.servlet.error.message
    public static final String ERROR_REQUEST_URI = RequestDispatcher.ERROR_REQUEST_URI;         // javax.servlet.error.request_uri
    public static final String ERROR_SERVLET_NAME = RequestDispatcher.ERROR_SERVLET_NAME;       // javax.servlet.error.servlet_name
    public static final String ERROR_STATUS_CODE = RequestDispatcher.ERROR_STATUS_CODE;         // javax.servlet.error.status_code

    @RequestMapping("/error-page/404")
    public String errorPage404(HttpServletRequest request, HttpServletResponse response) {
        log.info("errorPage404");
        printErrorInfo(request);
        return "/error-page/404";
    }

    // client 의 Accept 헤더 값이 "application/json" 가 아닌 경우 아래 메서드 호출 (json 아니라 html 응답, REST API 처리 불가)
    @RequestMapping("/error-page/500")
    public String errorPage500(HttpServletRequest request, HttpServletResponse response) {
        log.info("errorPage500");
        printErrorInfo(request);
        return "/error-page/500";
    }

    // client 의 Accept 헤더 값이 "application/json" 인 경우 아래 메서드 호출 (json 응답, REST API 처리 가능)
    @RequestMapping(value = "/error-page/500", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> errorPage500Api(
            HttpServletRequest request, HttpServletResponse response) {

        log.info("errorPage500Api");

        Integer statusCode = (Integer) request.getAttribute(ERROR_STATUS_CODE);
        Exception ex = (Exception) request.getAttribute(ERROR_EXCEPTION);
        String exMessage = ex.getMessage();

        Map<String, Object> result = new HashMap<>();
        result.put("status", statusCode);
        result.put("message", exMessage);

        return new ResponseEntity<>(result, HttpStatus.valueOf(statusCode));
    }

    private void printErrorInfo(HttpServletRequest request) {
        log.info("ERROR_EXCEPTION: {}", request.getAttribute(ERROR_EXCEPTION));
        log.info("ERROR_EXCEPTION_TYPE: {}", request.getAttribute(ERROR_EXCEPTION_TYPE));
        log.info("ERROR_MESSAGE: {}", request.getAttribute(ERROR_MESSAGE));
        log.info("ERROR_REQUEST_URI: {}", request.getAttribute(ERROR_REQUEST_URI));
        log.info("ERROR_SERVLET_NAME: {}", request.getAttribute(ERROR_SERVLET_NAME));
        log.info("ERROR_STATUS_CODE: {}", request.getAttribute(ERROR_STATUS_CODE));

        // dispatchType : 요청 타입 (고객 직접 요청, WAS 가 오류 페이지 요청, 서블릿이 다른 서블릿을 요청, 비동기 호출 등)
        // (중요) dispatchType=REQUEST : 고객이 직접 요청
        // (중요) dispatchType=ERROR : WAS 가 내부적으로 오류 페이지를 요청
        // dispatchType=FORWARD : 서블릿이 다른 서블릿을 요청
        // dispatchType=INCLUDE : 서블릿이 다른 서블릿을 포함
        // dispatchType=ASYNC : 서블릿 비동기 호출
        log.info("dispatchType={}", request.getDispatcherType());
    }
}
