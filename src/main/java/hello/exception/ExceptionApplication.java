package hello.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// spring-boot 는 오류 페이지를 반환하기 위해 /error 경로를 처리하는 BasicErrorController 를 호출한다
// BasicErrorController 는 아래의 우선순위로 오류 페이지 뷰를 찾는다
// 1. (뷰템플릿) resources/templates/error/오류코드.html
// 2. (정적리소스) resources/static 또는 public/error/오류코드.html
// 3. (정적리소스) resources/templates/error.html
// 5xx.html 과 같이 지정하면 500대 오류를 처리

@SpringBootApplication
public class ExceptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptionApplication.class, args);
	}

}
