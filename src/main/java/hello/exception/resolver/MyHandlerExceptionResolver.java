package hello.exception.resolver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// resolveException 사용 방법
// 1. response 에 원하는 형태의 error 담고(sendError) 빈 ModelAndView 반환 -> WAS 가 에러 페이지 응답
// 2. 에러 페이지 ModelAndView 반환 -> WAS 경유하지 않고 직접 에러 페이지 응답
// 3. response 에 정상적인 json 값 담고 빈 ModelAndView 반환 -> 정상 json 응답 전달
// 4. 정상적인 ModelAndView 반환 -> 정상 화면 렌더링
// 5. null 반환 -> 해당 Exception 처리 포기, 다른 ExceptionResolver 들도 처리 못하면 500 에러 전달

@Slf4j
public class MyHandlerExceptionResolver implements HandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (ex instanceof IllegalArgumentException) {
            log.info("MyHandlerExceptionResolver resolve IllegalArgumentException to 400 error");
            try {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
            } catch (IOException e) {
                log.error("MyHandlerExceptionResolver exception");
            }
            return new ModelAndView();
        }
        return null;
    }
}
