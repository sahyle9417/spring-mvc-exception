package hello.exception.resolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import hello.exception.exception.UserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

// resolveException 사용 방법
// 1. response 에 원하는 형태의 error 담고(sendError) 빈 ModelAndView 반환 -> WAS 가 에러 페이지 응답
// 2. 에러 페이지 ModelAndView 반환 -> WAS 경유하지 않고 직접 에러 페이지 응답
// 3. response 에 정상적인 json 값 담고 빈 ModelAndView 반환 -> 정상 json 응답 전달
// 4. 정상적인 ModelAndView 반환 -> 정상 화면 렌더링
// 5. null 반환 -> 해당 Exception 처리 포기, 다른 ExceptionResolver 들도 처리 못하면 500 에러 전달

@Slf4j
public class UserHandlerExceptionResolver implements HandlerExceptionResolver {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        try {
            if (ex instanceof UserException) {
                log.info("UserHandlerExceptionResolver resolve UserException to 400");
                String acceptHeader = request.getHeader("accept");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                // JSON (application/json)
                if (acceptHeader.equals("application/json")) {

                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");

                    Map<String, Object> errorMap = new HashMap<>();
                    errorMap.put("ex", ex.getCause());
                    errorMap.put("message", ex.getMessage());
                    String errorJson = objectMapper.writeValueAsString(errorMap);
                    response.getWriter().write(errorJson);

                    // 빈 ModelAndView 반환했으니 렌더링 수행X
                    return new ModelAndView();
                }
                // TEXT/HTML (text/html)
                else {
                    // spring 의 default error 페이지 경로를 직접 반환
                    // WAS 경유해서 다시 error 페이지 조회하는 복잡한 절차 없어짐
                    return new ModelAndView("error/500");
                }
            }
        } catch (Exception e) {
            log.error("UserHandlerExceptionResolver exception");
        }
        return null;
    }
}
