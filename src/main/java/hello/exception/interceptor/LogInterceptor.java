package hello.exception.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Slf4j
public class LogInterceptor implements HandlerInterceptor {

    public static final String LOG_ID = "logId";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        String uuid = UUID.randomUUID().toString();
        request.setAttribute(LOG_ID, uuid);
        log.info("[{}] [{}] [{}] LogInterceptor preHandle", requestURI, uuid, handler);

        // 정상 호출인 경우 true 를 반환하며 이후 다음 interceptor 또는 controller 가 호출됨
        return true;
    }

    // controller 정상 종료 후 view render 전에 postHandle 호출 (예외 발생 시 호출되지 않음)
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String requestURI = request.getRequestURI();
        String uuid = (String) request.getAttribute(LOG_ID);
        log.info("[{}] [{}] [{}] LogInterceptor postHandle (modelAndView={})", requestURI, uuid, handler, modelAndView);
    }

    // 요청 처리가 완전히 끝난 후 afterCompletion 호출 (예외 발생되어도 afterCompletion 은 무조건 호출됨)
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        String requestURI = request.getRequestURI();
        String uuid = (String) request.getAttribute(LOG_ID);
        log.info("[{}] [{}] [{}] LogInterceptor afterCompletion", requestURI, uuid, handler);
        if (ex != null) {
            log.error("[{}] [{}] [{}] LogInterceptor afterCompletion found error", requestURI, uuid, handler, ex);
        }
    }
}
